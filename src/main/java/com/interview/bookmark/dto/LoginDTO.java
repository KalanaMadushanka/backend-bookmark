package com.interview.bookmark.dto;

import lombok.Data;

@Data
public class LoginDTO {
    private String userName;
    private String password;
}
