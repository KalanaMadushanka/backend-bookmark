package com.interview.bookmark.repository;

import com.interview.bookmark.entity.Bookmark;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BookmarkRepository extends JpaRepository<Bookmark,Integer> {

    @Query("SELECT count(c.id) FROM Bookmark c")
    long getTotalBookmarks();


}
