package com.interview.bookmark.entity;


import javax.persistence.*;

@Entity
@Table(name = "bookmarks")
public class Bookmark {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "URL")
    private String url;
    @Column(name = "DESCRIPTION",length=512)
    private String description;
    @Column(name = "STATUS")
    private Integer status;
    @Column(name = "EXPIRY_DATE")
    private String expiry_date;
    @Column(name = "FEATURED")
    private Integer featured;
    @Column(name = "USER_ID")
    private Integer user_id;
    @Column(name = "IMAGE")
    private String image;
    @Column(name = "CREATED_AT")
    private String created_at;

    public Bookmark() {
    }

    public Bookmark(Integer id, String name, String url, String description, Integer status, String expiry_date, Integer featured, Integer user_id, String image, String created_at) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.description = description;
        this.status = status;
        this.expiry_date = expiry_date;
        this.featured = featured;
        this.user_id = user_id;
        this.image = image;
        this.created_at = created_at;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public Integer getFeatured() {
        return featured;
    }

    public void setFeatured(Integer featured) {
        this.featured = featured;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
