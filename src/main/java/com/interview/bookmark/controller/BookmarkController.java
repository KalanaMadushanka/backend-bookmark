package com.interview.bookmark.controller;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.interview.bookmark.dto.BookmarkDTO;
import com.interview.bookmark.service.BookmarkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("${app.endpoint.api}/bookmark")
public class BookmarkController {

    final Logger logger = LoggerFactory.getLogger(BookmarkController.class);

    @Autowired
    private BookmarkService bookmarkService;

    private final RestTemplate restTemplate;

    public BookmarkController(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @PostMapping("getBookMarkValues/")
    public void loadBookmarks() throws IOException {

        TypeReference<List<BookmarkDTO>> typeReference = new TypeReference<List<BookmarkDTO>>() {
        };
        URL url = new URL("http://demo.dreamsquadgroup.com/test/index.json");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("accept", "application/json");

        InputStream responseStream = connection.getInputStream();
        ObjectMapper mapper = new ObjectMapper();

        try {
            List<BookmarkDTO> bookmarks = mapper.readValue(responseStream, typeReference);
            bookmarkService.save(bookmarks);
            System.out.println("bookmark Saved!");
        } catch (IOException e) {
            System.out.println("Unable to save bookmark: " + e.getMessage());
            logger.error("Error when saving bookmark data : " + e);
        }
    }

    @PostMapping("/saveBookmark")
    public boolean saveBookmarkDetails(@RequestBody BookmarkDTO bookmarkDTO) {
        try {
            return bookmarkService.saveBookMarks(bookmarkDTO);
        } catch (Exception e) {
            logger.error("Error when saving bookmark data : " + e);
            return false;
        }
    }

    @DeleteMapping("/deleteBookmark/{id}")
    public boolean deleteBookmark(@RequestParam("id") Integer id) {
        return bookmarkService.deleteBookmark(id);
    }

    @GetMapping("/getAllBookmarks/")
    public ArrayList<BookmarkDTO> getAllBookmarks() {

        return bookmarkService.getAllBookmarks();
    }

    @GetMapping("/count")
    public long getBookmarkCount() {
        return bookmarkService.getTotalBookmarks();
    }
}
