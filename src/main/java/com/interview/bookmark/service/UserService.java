package com.interview.bookmark.service;


import com.interview.bookmark.dto.UserDTO;

import java.util.List;

public interface UserService {


    boolean checkUserNameExist(String userName);

    boolean checkNICExist(String nic);

    List<UserDTO> getAllUsers();


    UserDTO getUserDetails(String id);
}
