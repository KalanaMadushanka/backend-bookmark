package com.interview.bookmark.service.impl;


import com.interview.bookmark.dto.LoginDTO;
import com.interview.bookmark.dto.UserDTO;
import com.interview.bookmark.entity.User;
import com.interview.bookmark.repository.UserRepository;
import com.interview.bookmark.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@Transactional
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDTO checkUser(LoginDTO loginDTO) {
        if (loginDTO != null) {
            User byUserNameAndPassword = userRepository.findByUsernameAndPassword(loginDTO.getUserName(),
                    loginDTO.getPassword());
            if (byUserNameAndPassword != null) {
                UserDTO userDTO = new UserDTO();
                userDTO.setContactNo(byUserNameAndPassword.getContactNo());
                userDTO.setEmail(byUserNameAndPassword.getEmail());
                userDTO.setName(byUserNameAndPassword.getName());
                userDTO.setNic(byUserNameAndPassword.getNic());
                userDTO.setUserId(byUserNameAndPassword.getId());
                userDTO.setUsername(byUserNameAndPassword.getUsername());
                return userDTO;
            }else {
               return null;
            }
        }else {
            return null;
        }
    }

    @Override
    public boolean saveUserDetails(UserDTO userDTO) {
        if (userDTO != null) {
            User user = new User();
            user.setContactNo(userDTO.getContactNo());
            user.setEmail(userDTO.getEmail());
            user.setName(userDTO.getName());
            user.setNic(userDTO.getNic());
            user.setPassword(userDTO.getPassword());
            user.setUsername(userDTO.getUsername());
            user.setAddress(userDTO.getAddress());
            userRepository.save(user);
            return true;
        }else {
            return false;
        }
    }

    @Override
    public boolean forgotPassword(LoginDTO loginDTO) {
        if (loginDTO != null) {
            User byUserName = userRepository.findByUsername(loginDTO.getUserName());
            byUserName.setPassword(loginDTO.getPassword());
            userRepository.save(byUserName);
            return true;
        }else {
            return false;
        }
    }
}
