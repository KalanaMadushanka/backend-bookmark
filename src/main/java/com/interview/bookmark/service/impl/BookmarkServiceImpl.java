package com.interview.bookmark.service.impl;

import com.interview.bookmark.dto.BookmarkDTO;
import com.interview.bookmark.entity.Bookmark;
import com.interview.bookmark.repository.BookmarkRepository;
import com.interview.bookmark.service.BookmarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class BookmarkServiceImpl implements BookmarkService {

    @Autowired
    private BookmarkRepository bookmarkRepository;

    @Override
    public ArrayList<BookmarkDTO> getAllBookmarks() {

        List<Bookmark> bookmarkDTOS = bookmarkRepository.findAll();
        ArrayList<BookmarkDTO> alBookmarks = new ArrayList<>();
        for (Bookmark bookmark : bookmarkDTOS) {
            BookmarkDTO bookmarkDTO = new BookmarkDTO(bookmark.getId(), bookmark.getName(),bookmark.getUrl(),bookmark.getDescription(),bookmark.getStatus(),bookmark.getExpiry_date(),bookmark.getFeatured(),bookmark.getUser_id(),bookmark.getImage(),bookmark.getCreated_at());
            alBookmarks.add(bookmarkDTO);
        }

        return alBookmarks;
    }

    @Override
    public boolean saveBookMarks(BookmarkDTO bookmarkDTO) {
        Bookmark bookmark = new Bookmark();
        bookmark.setId(bookmarkDTO.getId());
        bookmark.setName(bookmarkDTO.getName());
        bookmark.setUrl(bookmarkDTO.getUrl());
        bookmark.setDescription(bookmarkDTO.getDescription());
        bookmark.setStatus(bookmarkDTO.getStatus());
        bookmark.setExpiry_date(bookmarkDTO.getExpiry_date());
        bookmark.setFeatured(bookmarkDTO.getFeatured());
        bookmark.setUser_id(bookmarkDTO.getUser_id());
        bookmark.setImage(bookmarkDTO.getImage());
        bookmark.setCreated_at(bookmarkDTO.getCreated_at());
        bookmarkRepository.save(bookmark);
        return true;
    }

    @Override
    public boolean deleteBookmark(Integer id) {
        bookmarkRepository.deleteById(id);
        return true;
    }

    @Override
    public long getTotalBookmarks() {
        return bookmarkRepository.getTotalBookmarks();
    }

    @Override
    public Iterable<BookmarkDTO> save(List<BookmarkDTO> bookmarkDTOS) {
        for (BookmarkDTO bookmarkDTO : bookmarkDTOS) {
            Bookmark bookmark = new Bookmark();
            bookmark.setId(bookmarkDTO.getId());
            bookmark.setName(bookmarkDTO.getName());
            bookmark.setUrl(bookmarkDTO.getUrl());
            bookmark.setDescription(bookmarkDTO.getDescription());
            bookmark.setStatus(bookmarkDTO.getStatus());
            bookmark.setExpiry_date(bookmarkDTO.getExpiry_date());
            bookmark.setFeatured(bookmarkDTO.getFeatured());
            bookmark.setUser_id(bookmarkDTO.getUser_id());
            bookmark.setImage(bookmarkDTO.getImage());
            bookmark.setCreated_at(bookmarkDTO.getCreated_at());
            bookmarkRepository.save(bookmark);
        }

        return bookmarkDTOS;
    }
}
