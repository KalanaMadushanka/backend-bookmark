package com.interview.bookmark.service.impl;

import com.interview.bookmark.dto.UserDTO;
import com.interview.bookmark.entity.User;
import com.interview.bookmark.repository.UserRepository;
import com.interview.bookmark.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public boolean checkUserNameExist(String userName) {
        User byUserName = userRepository.findByUsername(userName);
        if (byUserName != null) {
            return true;
        }else {
            return false;
        }
    }

    @Override
    public boolean checkNICExist(String nic) {
        User byNic = userRepository.findByNic(nic);
        if (byNic != null) {
            return true;
        }else {
            return false;
        }
    }

    @Override
    public List<UserDTO> getAllUsers() {
        List<UserDTO> list = new ArrayList<>();
        List<User> all = userRepository.findAll();

        for (User user:all) {
            UserDTO userDTO = new UserDTO();
            userDTO.setUserType(user.getUsername());
            userDTO.setNic(user.getNic());
            userDTO.setName(user.getName());
            userDTO.setEmail(user.getEmail());
            userDTO.setContactNo(user.getContactNo());
            userDTO.setUserId(user.getId());
            userDTO.setAddress(user.getAddress());
            list.add(userDTO);
        }
        return list;
    }



    @Override
    public UserDTO getUserDetails(String id) {
        User user = userRepository.findById(Integer.valueOf(id)).get();
        UserDTO userDTO = new UserDTO();
        userDTO.setAddress(user.getAddress());
        userDTO.setUserId(user.getId());
        userDTO.setContactNo(user.getContactNo());
        userDTO.setEmail(user.getEmail());
        userDTO.setName(user.getName());
        userDTO.setNic(user.getNic());
        userDTO.setUsername(user.getUsername());
        userDTO.setPassword(user.getPassword());
        return userDTO;
    }



}
