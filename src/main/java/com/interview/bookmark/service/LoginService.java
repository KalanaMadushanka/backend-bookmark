package com.interview.bookmark.service;


import com.interview.bookmark.dto.LoginDTO;
import com.interview.bookmark.dto.UserDTO;

public interface LoginService {
    UserDTO checkUser(LoginDTO loginDTO);

    boolean saveUserDetails(UserDTO userDTO);

    boolean forgotPassword(LoginDTO loginDTO);
}
