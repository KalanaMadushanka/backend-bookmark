package com.interview.bookmark.service;

import com.interview.bookmark.dto.BookmarkDTO;
import com.interview.bookmark.entity.Bookmark;


import java.util.ArrayList;
import java.util.List;


public interface BookmarkService {

    ArrayList<BookmarkDTO> getAllBookmarks();

    public boolean saveBookMarks(BookmarkDTO bookmarkDTO);

    public boolean deleteBookmark(Integer id);

    public long getTotalBookmarks();

    public Iterable<BookmarkDTO> save(List<BookmarkDTO> bookmarkDTOS);


}
